# Effective models

In this repository we keep short codes with the group theory derivation of relevant effective models.

## Status for each model

- **Graphene:** to do...
- **BHZ:** initial code ready, needs discussions and details.
- **Haldane:** needs revision, is it correct? Should there be a $`\sigma_x`$ constant mass term?
- **Kane-Mele:** to do...
- **Rashba 2DEG:** to do...
